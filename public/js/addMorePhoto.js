function addMorePhoto(){
    var oldNumOfPhoto = document.getElementById('oldNumOfPhoto').value;
    var newNumOfPhoto = document.getElementById('newNumOfPhoto').value;
    if(newNumOfPhoto==3){
        alert("Kuvien maksimimäärä savutettu.");
        return;
    }
    if(oldNumOfPhoto!=newNumOfPhoto && (document.getElementById("photo".concat(newNumOfPhoto)).files.length==0)){
        alert("Edellinen lisätty kuva ei ole vielä ladattu, voit lisätä uusia kuvia vasta ladattuasi sen.");
    }
    else{
        newNumOfPhoto++;
        document.getElementById('newNumOfPhoto').value = newNumOfPhoto;
        var addPhotoDiv=document.createElement('div');
        addPhotoDiv.className="form-group";
        addPhotoDiv.innerHTML="<label class=\"w3-left\" for=\"photo".concat(newNumOfPhoto) + "\">Lataa kuva: </label>" +
        "<input class=\"w3-right\" type=\"text\" name=\"alt".concat(newNumOfPhoto)   +"\" id=\"alt".concat(newNumOfPhoto) + "\" required>"+
        "<label class=\"w3-right\" for=\"alt".concat(newNumOfPhoto) +"\">Kuvan nimi: </label>" +
        "<input class=\"w3-left\" type=\"file\" accept=\"image/*\" name=\"photo".concat(newNumOfPhoto) +"\" id=\"photo".concat(newNumOfPhoto) +"\"><br>" ;
        document.getElementById('addPhoto').appendChild(addPhotoDiv);  
    }
}
