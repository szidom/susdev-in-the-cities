This is a project to make a website in Laravel as my traineeship.

Set Up the project;
1. You will need to have installed on your computer:
    sudo apt-get install apache2
    sudo apt-get install mysql-server
    sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
    install phpmysql...
    
    PHP, Mysql, composer, laravel,
    
    on server(PHP >= 5.6.4
    OpenSSL PHP Extension
    PDO PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
    Composer)
    

2. then pull the whole project to some directory
3. then create the next folder hierarhy outside of the project:
    *commonfiles->
        *avatars
        *permaculture
        *projects
        *references
        *designs
  then make a link on that and copy it into both project in the (projecName)/storage/app/public folder
  .env file configuralasa( 
        1st dbconnection
        2nd google capcha parts on the bottom of the .env,
        and so on)

4. phpmyadmin susdev_db create, 
 
5. then commands on terminal in susdev-in-cities folder:
    - composer update 
    - composer dump-autoload
    - php artisan storage:link
    - php artisan key:generate 
    .env setup
    - php artisan migrate
    - php artisan db:seed
 
6. then commands on terminal in susdev-backend folder:
    - composer update
    - composer dump-autoload
    - php artisan storage:link
    - php composer.phar require intervention/image
    - php artisan db:seed
    
by Szilveszter Domány
