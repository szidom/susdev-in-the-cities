<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@dashboard')->name('home');

Route::get('/references', 'ReferenceController@getReferences');
Route::get('/references/{ref}', 'ReferenceController@show');
Route::get('/intro', 'InformationPageController@listingInfos');
Route::get('/designers', 'HomeController@listingDesigners');

Auth::routes();

Route::get('/assignment', 'ProjectController@showAddPage');
Route::post('/loggedIn/assign', 'ProjectController@add');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/designers/{id}', 'DesignerController@showDesigner');

Route::get('projects', 'ProjectController@index');
Route::patch('projects/update/{project}','ProjectController@update');

Route::delete('/photos/picremove/{photo}', 'PhotoController@destroy');
Route::get('/home', 'HomeController@dashboard');

Route::get('/settings', 'UserController@index');
Route::patch('/settings/{user}', 'UserController@update');
Route::delete('/settings/removeAvatar/{photo}', 'UserController@removeAvatar');

Route::get('/designs/{design}','ReferenceController@showDesign');
Route::get('/designs', 'ReferenceController@getDesigns');

Route::get('/contact', 'ContactController@create');
Route::post('/contact/new', 'ContactController@store');
