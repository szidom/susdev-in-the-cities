@extends('layouts.frontend')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="w3-container">
    @foreach($projects as $project)    
    @if($project->onGoing && !$project->isNew)
    <div class="alert alert-warning">
        <strong>Suunnitelmaa työstetään!</strong> Jos muutat tehtävänantoa, niin projekti palaa <strong>uusi</strong> vaiheeseen.
    </div>
    @endif
    @if($project->Design()->count())
    <div class="form-group w3-center">
        <button type="button" class="btn btn-info" 
                onclick="javascript:location.href='/designs/{{$project->Design()->first()->id}}'">Siirry suunnitelmaan</button>  
    </div>
    @endif
    <form method="post" name="assignForm" enctype="multipart/form-data" action="/projects/update/{{$project->id}}" class="form">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Projektin nimi</label>
            <input type="text" name="name" class="form-control" required value="{{$project->name}}">       
        </div>
        <div class="form-group">
            <label for="description">Projektin kuvaus</label>
            <textarea name="description" rows=7 class="form-control" id="texteditor" required>{{$project->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="loc">Sijainti</label>
            <div class="form-control" name="loc" id="map"></div>
        </div>
        <div class="form-group">
            <input type="text" id="add" name="address" class="w3-text col-md-9" value="{{$project->Location()->first()->address}}">
            <button type="button" class="btn btn-info w3-right" onclick="codeAddress()">Etsi kartalta</button>   
            <input type="hidden" id="lat" name="lat" class="form-control" value={{$project->Location()->first()->lat}} >
            <input type="hidden" id="lng" name="lng" class="form-control" value={{$project->Location()->first()->lng}} >
            <input type="hidden" id="oldNumOfPhoto" class="form-control" value={{$project->Photos()->count()}} >
            <input type="hidden" id="newNumOfPhoto" name="newNumOfPhoto" class="form-control" value={{$project->Photos()->count()}} >
        </div>
        @if($project->Photos()->count())
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-12">
                        <label>Kuvat</label>
                    </div>
                    @foreach($project->Photos()->get() as $photo)
                        <div class="col-md-4" >
                            <a href="#picremove_{{$photo->id}}" class="w3-right">
                            <span data-toggle="modal"
                                data-target="#picremove_{{$photo->id}}"
                                title="Remove {{$photo->alt}}" class="fa fa-trash">
                            </span>
                            </a>
                            <a href="/{{$photo->location}}">
                            <img class="w3-image-small" src="/{{$photo->location}}" title="Open {{$photo->alt}}"  alt="{{$photo->alt}}" />
                            </a>
                        </div>                           
                    @endforeach
                </div>
            </div>
            <hr>
        @endif 
        
        <div class="form-group" id="addPhoto">
            <label class="panel-body">Lataa kuvia 1-3 kpl (max: 2 Mb)</label>            
        </div>
        <div class="form-group panel-body"> 
            <a href="#" class="w3-left" onclick="addMorePhoto()"><span class="fa fa-plus-circle" aria-hidden="true"></span> Lisää kuva</a>          
            <button type="submit" class="btn btn-success w3-right">Tallenna muutokset</button>      
        </div>
    </form>    
    @foreach($project->Photos()->get() as $photo)
    <div class="modal fade" id="picremove_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" rofutoversenzle="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Poista {{$photo->alt}}</h4>
                </div>
                <div class="modal-body">
                    Haluatko varmasti poistaa kuvan?
                </div>
                <div class="modal-footer">
                <button type="button" 
                    class="btn btn-default" 
                    data-dismiss="modal"
                    style="float:left;">Peruuta</button>
                <form 	method="post" 
                        action="/photos/picremove/{{$photo->id}}">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button type="submit" value="delete" class="btn btn-danger">poista</button>
                </form>
                </div>
            </div>    		
        </div>
    </div>	
    @endforeach    
    @endforeach  
        <div class="panel-body w3-center">
         <br><br>
            <div class="navbar-fixed-bottom center">{{ $projects->links() }}</div>
        </div>
</div>
<script type="text/javascript" src="{{ URL::asset('js/addMorePhoto.js') }}"></script>
<script>
    var geocoder;
    var map;
    var marker;
    
    function initMap() {
        var loclat = parseFloat(document.getElementById('lat').value);
        var loclng = parseFloat(document.getElementById('lng').value);
    
        var spot = {
            lat: loclat, 
            lng: loclng
        };
        
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: spot
        });
        
        marker = new google.maps.Marker({
            map: map,
            draggable: true,    
            position: spot
        });
        
        geocoder = new google.maps.Geocoder();
        
        google.maps.event.addListener(map, "rightclick", function(event) {
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            var latlng = new google.maps.LatLng(lat, lng);
            marker.setPosition(latlng);
            // populate yor box/field with lat, lng
            //alert("Lat=" + lat + "; Lng=" + lng);
            document.getElementById("lat").setAttribute("value", lat);
            document.getElementById("lng").setAttribute("value", lng);
            document.getElementById("add").setAttribute("value", marker.getTitle());
            geocodePosition(latlng);
        });
        
    }
    
    function codeAddress() {
    var address = document.getElementById('add').value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        map.setCenter(results[0].geometry.location);
        marker.setPosition(results[0].geometry.location);   
        document.getElementById("lat").setAttribute("value", lat);
        document.getElementById("lng").setAttribute("value", lng); 
        document.getElementById("add").setAttribute("value", marker.getTitle());    
        
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
    
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
              document.getElementById("add").setAttribute("value", responses[0].formatted_address); 
            } else {
                document.getElementById("add").setAttribute("value",'Cannot determine address at this location.');
            }
        });
    }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7bZOjzEEMTiymu0GqsZOv9NGgYCH7fr4&callback=initMap">
</script>
@endsection
