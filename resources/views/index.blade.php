@extends('layouts.frontend')

@section('content')

    <div class="container">
        <h1 class="display-4">{{$intro->title}}</h1>
        <p>{{$intro->content}}</p>
        <p><a class="btn btn-success btn-lg" href="/intro" role="button">Learn more &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
      @foreach($refs as $ref)
        <div class="col-md-4">        
          <h2>{{$ref->name}}</h2>
          <p>{{$ref->description}}</p>
          <p><a class="btn btn-secondary" href="/references/{{$ref->id}}" role="button">View details &raquo;</a></p>
        </div>
        @endforeach
      </div>
    </div>
      <hr>
@endsection
      
