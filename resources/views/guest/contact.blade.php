@extends('layouts.frontend')

@section('content')
<div class="container">


    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
            @if ($errors->any())
                <div class="alert alert-info">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <h2 class="panel-heading">Yhteydenotto</h2>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/contact/new">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('fromTitle') ? ' has-error' : '' }}">
                            <label for="fromTitle" class="col-md-4 control-label">Sähköposti</label>

                            <div class="col-md-6">
                                <input id="fromTitle" type="email" class="form-control" name="fromTitle" value="{{ old('fromTitle') }}" required autofocus>

                                @if ($errors->has('fromTitle'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fromTitle') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject" class="col-md-4 control-label">Aihe</label>

                            <div class="col-md-6">
                                <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" required >

                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="message" class="col-md-4 control-label">Viesti</label>

                            <div class="col-md-6">
                                <textarea rows=5 id="message" type="text" class="form-control" name="message" required>{{ old('message') }}</textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="g-recaptcha" name="g-recaptcha" data-sitekey="6LcDOiwUAAAAAGLiIQzqfZ3Q21NZyWIQ9lqMphuI"></div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Lähetä
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
