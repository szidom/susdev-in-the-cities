@extends('layouts.frontend')

@section('content')
<div class="container">
    
        <div class="panel-heading h1">Johdatus Permakulttuuriin</div>
        
        <hr> 
        <div class="w3-container">  
        @if($info_pages->isEmpty())
                  
                    <h2 class="w3-text">Under Construction...</h2>      
        
        @else
            @foreach($info_pages as $info)            
                <div class="w3-container row">        
                    <h2 class="w3-text col-md-12">{{$info->title}}</h2>
                    
                    <p class="col-md-12">{!!$info->content!!}   </p>          
                </div>
                <hr>
            @endforeach
        @endif
        </div>
</div>
@stop
