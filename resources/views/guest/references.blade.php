@extends('layouts.frontend')

@section('content')

<div class="w3-container">
    <div class="w3-container">
        <h2 class="w3-text">Projektit</h2>
        <hr>
    </div>
    <div class="album text-muted">
        <div class="w3-container">
            <div class="w3-row">
            @foreach($references as $ref)
                <div class="w3-card card">
                    <a class="w3-center nav-link" href="/references/{{$ref->id}}">
                        <h3 class="w3-text">{{ $ref->name }}</h3>
                        @if($ref->Photos()->count())
                        <img class="w3-image-album" src="/{{$ref->Photos()->first()->location}}"   alt="{{$ref->Photos()->first()->alt}}"/>
                        @endif
                        <p class="card-text w3-text">{{ $ref->description }}</p>
                    </a>
                </div>        
            @endforeach
          </div>  
        </div>
    </div>
    <hr>
</div>

@stop
