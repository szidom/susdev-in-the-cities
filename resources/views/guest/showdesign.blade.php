@extends('layouts.frontend')

@section('content')

<div class="container">
    <div class="row w3-center">
        <h2 class="w3-text col-md-8">Design for {{$ref->name}}
            <a class="w3-right" href="/references/{{$ref->id}}"><span class="fa fa-exchange"></span></a>
        </h2>
    </div>
    <div class="w3-container w3-center">
        <hr>
        @if($design->Photos()->count())
        <img class="w3-image" src="/{{$design->Photos()->first()->location}}" alt="{{$design->Photos()->first()->alt}}">
        @endif
        <p class="w3-text w3-justify">{{ $design->description }}</p>
        <div class="container">
            <div class="w3-row">
                @foreach($design->Photos()->where('number','>','1')->get() as $photo)
                <div class="w3-card card">
                    <a href="#picopen_{{$photo->id}}" data-toggle="modal"
                                data-target="#picopen_{{$photo->id}}"
                                title="Open {{$photo->alt}}">
                        <img class="w3-image-album" src="/{{$photo->location}}" alt="{{$photo->alt}}">
                    </a>
                </div>        
                <div class="modal fade" id="picopen_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" rofutoversenzle="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">{{$photo->alt}}</h4>
                                </div>
                                <div class="modal-body w3-center">
                                    <img class="w3-image" src="/{{$photo->location}}" alt="{{$photo->alt}}"><br>
                                </div>
                                <div class="modal-footer">
                                <button type="button" 
                                    class="btn btn-default" 
                                    data-dismiss="modal"
                                    style="float:left;">Close</button>
                                </div>
                            </div>    		
                        </div>
                </div>        
                @endforeach
            </div>
        </div>
        <p class="w3-right w3-text">Designed by
            <a href="/designers/{{$ref->designer}}">{{$ref->Designer()->first()->name}}</a>
        <p>
    </div>
    <br>
</div>

@stop
