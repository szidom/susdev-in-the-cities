@extends('layouts.frontend')

@section('content')

<div class="container">
    <div class="row w3-center">
        <h2 class="w3-text col-md-8">{{$ref->name}}</h2>
        @if(!empty($design) && !$ref->onGoing)
            <a href="/designs/{{$design->id}}" class="col-md-4">
                <h3 class="w3-left">Näytä suunnitelma </h3>
                <span class="fa fa-exchange fa-2x"></span>
            </a>
        @endif
    </div>
    <div class="w3-container w3-center">
        <hr>
        @if($ref->Photos()->count())
        <img class="w3-image" src="/{{$ref->Photos()->first()->location}}" alt="{{$ref->Photos()->first()->alt}}">
        @endif
        <p class="w3-text w3-justify">{{ $ref->description }}</p>
        <div class="container">
            <div class="w3-row row">
                @foreach($ref->Photos()->where('number','>','1')->get() as $photo)
                <div class="w3-card card">
                    <a href="#picopen_{{$photo->id}}" data-toggle="modal"
                                data-target="#picopen_{{$photo->id}}"
                                title="Open {{$photo->alt}}">
                        <img class="w3-image-album" src="/{{$photo->location}}" alt="{{$photo->alt}}">
                    </a>
                </div>        
                <div class="modal fade" id="picopen_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" rofutoversenzle="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">{{$photo->alt}}</h4>
                                </div>
                                <div class="modal-body w3-center">
                                    <img class="w3-image" src="/{{$photo->location}}" alt="{{$photo->alt}}"><br>
                                </div>
                                <div class="modal-footer">
                                <button type="button" 
                                    class="btn btn-default" 
                                    data-dismiss="modal"
                                    style="float:left;">Close</button>
                                </div>
                            </div>    		
                        </div>
                </div>        
                @endforeach
            </div>
        </div>
        @if($ref->Location()->count())
        <div class="form-group">
            <div class="form-control" name="loc" id="map"></div>
        </div>
        <div class="form-group">
            <input type="text" name="address" class="form-control" readonly value="{{$ref->Location()->first()->address}}">
            <input type="hidden" id="lat" name="geolat" class="form-control" value={{$ref->Location()->first()->lat}}>
            <input type="hidden" id="lng" name="geolng" class="form-control" value={{$ref->Location()->first()->lng}}>            
        </div>
        @endif
        @if(!empty($ref->designer))
            <p class="w3-right w3-text">Designed by
                <a href="/designers/{{$ref->designer}}">{{$ref->Designer()->first()->name}}</a>
            <p>
        @endif
    </div>
    
</div>
<script>
function initMap(){
    var map;
    var marker;
    loclat = parseFloat(document.getElementById('lat').value);
    loclng = parseFloat(document.getElementById('lng').value);
    var spot = {lat: loclat, lng: loclng };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: spot
    });
    
    marker = new google.maps.Marker({
        map: map,   
    });
    marker.setPosition(spot);
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7bZOjzEEMTiymu0GqsZOv9NGgYCH7fr4&callback=initMap">
</script>
@stop
