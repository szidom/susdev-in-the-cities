@extends('layouts.frontend')

@section('content')

<div class="w3-container">
    <div class="w3-container">
        <h2 class="w3-text">Suunnitelmat</h2>
        <hr>
    </div>
    <div class="album">
        <div class="w3-container">
            <div class="w3-row">
            @foreach($references as $ref)
                <div class="w3-card card">
                    <a class="w3-center nav-link" href="/designs/{{$ref->id}}">
                        <h3 class="w3-text">{{ $ref->Project()->first()->name }}</h3>
                        @if($ref->Photos()->count())
                        <img class="w3-image-album" src="/{{$ref->Photos()->first()->location}}"   alt="{{$ref->Photos()->first()->alt}}"/>
                        @endif
                        <p class="card-text w3-text">{{ $ref->description }}</p>
                    </a>
                </div>        
            @endforeach
          </div>  
        </div>
    </div>
    <hr>
</div>

@stop
