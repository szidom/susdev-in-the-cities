@extends('layouts.frontend')

@section('content')

<div class="w3-container">
    <div class="w3-container">
        <h2 class="w3-text">Suunnittelijat</h2>
        <hr>
    </div>
        <div class="w3-container">
            <div class="w3-row">
            @foreach($designers as $designer)
                <div class="w3-card card">
                    <a class="w3-center nav-link" href="/designers/{{$designer->id}}">
                        <h3 class="w3-text">{{ $designer->name }}</h3>
                        @if(!empty($designer->Avatar()->first()))
                        <img class="w3-image-album" src="/{{$designer->Avatar()->first()->location}}"   alt="{{$designer->Avatar()->first()->alt}}">
                        @endif
                        <p class="card-text w3-text">{{ $designer->intro }}</p>
                    </a>       
               </div> 
            @endforeach
            </div>  
        </div>
    
</div>

@stop
