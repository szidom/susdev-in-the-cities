@extends('layouts.frontend')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach            
        </ul>
    </div>
@endif
<div class="w3-container">
    <form method="post" name="userForm" enctype="multipart/form-data" action="/settings/{{$user->id}}" class="form">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="row">
           <div class="col-md-8">
            <div class="form-group row">
                <label class="col-md-3" for="name">Käyttäjänimi</label>
                <input class="col-md-7" type="text" name="name" id="name" required value="{{empty(old('name'))? $user->name : old('name')}}"/>       
            </div>
            <div class="form-group row">
                <label class="col-md-3" for="email">Sähköposti</label>
                <input class="col-md-7" type="text" name="email" id="email" required value="{{empty(old('email'))? $user->email : old('email')}}">
            </div>
            <div class="form-group row">
                <label class="col-md-3" for="surname">Etunimi</label>
                <input class="col-md-7" type="text" name="surname" id="surname" required value="{{empty(old('surname'))? $user->surname : old('surname')}}"
                >
            </div>
            <div class="form-group row">
                <label class="col-md-3" for="lastname">Sukunimi</label>
                <input class="col-md-7" type="text" name="lastname" id="lastname" required value="{{empty(old('lastname'))? $user->lastname : old('lastname')}}">
            </div>
            <div class="form-group row">
                <label class="col-md-3" for="phone">Puhelin</label>
                <input class="col-md-7" type="text" name="phone" id="phone" value="{{empty(old('phone'))? $user->phone : old('phone')}}">
            </div>
            <div class="form-group row">
                <div class="alert alert-warning col-md-10">
                    <strong>Huomio!</strong> Tallentaaksesi muutokset sinun tulee kirjoittaa salasanasi. Voit myös vaihtaa salasanasi uuteen alla.
                </div>
                <label class="col-md-3" for="password">Nykyinen tunnusana</label>
                <input class="col-md-7" type="password" id="password" name="password" required>
            </div>    
            <div class="form-group row">
                <label class="col-md-3" for="new_password">Uusi tunnusana</label>
                <input class="col-md-7" type="password" name="new_password" id="new_password">
            </div>  
            <div class="form-group row">
                <label class="col-md-3" for="new_password_confirmation">Vahvista uusi tunnusana</label>
                <input class="col-md-7" type="password" name="new_password_confirmation" id="new_password_confirmation">
            </div>            
                      
          </div>
          <div class="col-lg-4 row">
            @if($user->Avatar()->count())
                @php
                    $photo = $user->Avatar()->first();
                @endphp
                <div class="form-group">
                    <a href="#picremove_{{$photo->id}}" class="w3-right">
                        <span data-toggle="modal"
                            data-target="#picremove_{{$photo->id}}"
                            title="Remove {{$photo->alt}}" class="fa fa-trash">
                        </span>
                    </a>
                    <a href="/{{$photo->location}}">
                    <img id="avatar" class="w3-image" src="{{$photo->location}}" title="Open" alt="{{$photo->alt}}">
                    </a>      
                </div>
                
                <hr>
            @endif 
            
            <div class="form-group">
                <label for="photo">
                    @if($user->Avatar()->count())
                        Vaihda kuva
                    @else
                        Lataa kuva
                    @endif
                </label>                
                <input  type="file" accept="image/*" name="photo" id="photo" > 
            </div>    
          </div>
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit" >Päivitä profiili</button>
        </div>
    </form>  
      
    @if($user->Avatar()->count())
    <div class="modal fade" id="picremove_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" rofutoversenzle="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Poista {{$photo->alt}}</h4>
                </div>
                <div class="modal-body">
                    Haluatko varmasti poistaa kuvan?
                </div>
                <div class="modal-footer">
                <button type="button" 
                    class="btn btn-default" 
                    data-dismiss="modal"
                    style="float:left;">Peruuta</button>
                <form 	method="post" 
                        action="/settings/removeAvatar/{{$photo->id}}">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button type="submit" value="delete" class="btn btn-danger">Poista</button>
                </form>
                </div>
            </div>    		
        </div>
    </div>
    @endif
</div>
@endsection
