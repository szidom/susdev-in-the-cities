@extends('layouts.frontend')

@section('content')

<div class="container row">
    <h2 class="w3-text col-lg-12">{{$designer->name}}</h2> 
            
    <div class="container col-lg-8">   
    <hr>             
        <h3 for="contact" class="w3-text">Yhteystiedot:</h3>
        <div id="contact" class="container">
            @if($designer->surname && $designer->lastname)
            <p class="w3-text" name="name">Nimi: {{$designer->surname}} {{$designer->lastname}}</p>
            @endif
            @if($designer->phone)
            <p class="w3-text" name="phone">Puhelin: {{$designer->phone}}</p>
            @endif
            <p class="w3-text" name="mail" >Sähköposti: {{$designer->email}}</p>
            @if($designer->intro)
            <label for="intro">Käyttöönotto:</label>
            <p class="w3-text col-md-12" name="intro" align=justify >{{ $designer->intro }}</p>   
            @endif        
        </div>
    </div>
    @if($designer->Avatar()->count())
    <div class="container col-lg-4">
        <img class="w3-image w3-right" src="/{{$designer->Avatar()->first()->location}}" alt="{{$designer->Avatar()->first()->alt}}">
    </div>
    @endif
    <div class="col-md-12">
        <button type="button" class="btn btn-primary w3-right" onclick="javascript:location.href='/designers'">
        Back to Designers Page
        </button>  
    </div>
</div>

@stop
