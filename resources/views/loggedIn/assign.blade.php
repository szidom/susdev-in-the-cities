@extends('layouts.frontend')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="w3-container">
    <form method="post" name="assignForm" enctype="multipart/form-data" action="/loggedIn/assign" class="form">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Projektin nimi</label>
            <input type="text" name="name" id="name" class="form-control" required value="{{ old('name') }}">       
        </div>
        <div class="form-group">
            <label for="description">Projektin kuvaus</label>
            <textarea name="description" rows=7 class="form-control" id="description" required>{{ old('description') }}</textarea>
        </div>
        
        <div class="form-group">
            <label for="loc">Sijainti</label>
            <div class="alert alert-warning">
            <strong>Huomio!</strong> Voit valita kohteen sijainnin suoraan kartalta, jolloin osoitekenttä jää tyhjäksi. <strong>Vaihtoehtona</strong> on kirjoittaa sijainti osoitekenttään kartan alle ja klikata etsi-painiketta, mutta tällöin sijaintia ei voi enää hakea suoraan kartalta.
        </div>
            <div class="form-control" name="loc" id="map"></div>
        </div>
        <div class="form-group">
            <input type="text" id="add" name="address" class="w3-text col-md-9" value="{{ old('address') }}">
            <button type="button" class="btn btn-info w3-right" onclick="codeAddress()">Etsi kartalta</button>   
            <input type="hidden" id="lat" name="geolat" class="form-control" value="{{ old('geolat') }}">
            <input type="hidden" id="lng" name="geolng" class="form-control" value="{{ old('geolng') }}">
            <input type="hidden" id="newNumOfPhoto" name="newNumOfPhoto" class="form-control" value="0" >
        </div>
        <div class="form-group" id="addPhoto">
            <label class="panel-body">Lataa kuvia palveluun 1-3 kpl (enimmäiskoko 2 Mb)</label>            
        </div>
        <div class="form-group"> 
            <a href="#" class="w3-left" onclick="addMorePhoto()"><span class="fa fa-plus-circle" aria-hidden="true"></span> Lisää kuva</a>          
            <button type="submit" class="btn btn-success w3-right">Lähetä</button>      
        </div>
    </form>    
</div>
<br>
<script type="text/javascript" src="{{ URL::asset('js/addPhoto.js') }}"></script>
<script>
    var geocoder;
    var map;
    var marker;
    
    function initMap() {
        var mapcenter;
        if(document.getElementById("lat").value && document.getElementById("lng"))
        {
            mapcenter={
                lat: parseFloat(document.getElementById("lat").value),
                lng: parseFloat(document.getElementById("lng").value)
            };
        } 
        else
            mapcenter = {lat: 60.432, lng: 22.084}; //Turku
        
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: mapcenter
        });      
        
        
        marker = new google.maps.Marker({
            map: map,   
            position: mapcenter 
        });
        
        geocoder = new google.maps.Geocoder();
        
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            mapcenter = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            map.setCenter(mapcenter);
            marker.setPosition(mapcenter);
            document.getElementById("lat").setAttribute("value", mapcenter.lat);
            document.getElementById("lng").setAttribute("value", mapcenter.lng);
            geocodePosition(mapcenter);
          });
        };       
        
        google.maps.event.addListener(map, "rightclick", function(event) {
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            var latlng = new google.maps.LatLng(lat, lng);
            marker.setPosition(latlng);
            // populate yor box/field with lat, lng
            //alert("Lat=" + lat + "; Lng=" + lng);
            document.getElementById("lat").setAttribute("value", lat);
            document.getElementById("lng").setAttribute("value", lng);
            //document.getElementById("add").setAttribute("value", marker.getTitle());
            geocodePosition(latlng);
        });
        
    }
    
    function codeAddress() {
    var address = document.getElementById('add').value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        map.setCenter(results[0].geometry.location);
        marker.setPosition(results[0].geometry.location);   
        document.getElementById("lat").setAttribute("value", lat);
        document.getElementById("lng").setAttribute("value", lng); 
        document.getElementById("add").setAttribute("value", marker.getTitle());    
        
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
    
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
              document.getElementById("add").setAttribute("value", responses[0].formatted_address); 
            } else {
                document.getElementById("add").setAttribute("value",'Cannot determine address at this location.');
            }
        });
    }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7bZOjzEEMTiymu0GqsZOv9NGgYCH7fr4&callback=initMap">
</script>
@endsection
