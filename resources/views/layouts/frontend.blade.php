<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
   <!--
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link rel="icon" href="../../favicon.ico"><span class="glyphicon glyphicon-leaf" aria-hidden="true"></span>
    
     CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Vihreä Kaupunki</title>

    <!-- Custom styles for this template 
    <link href="/css/w3.css" rel="stylesheet">-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS 
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">    
    <link href="{{asset('css/jumbotron.css')}}" rel="stylesheet">
  </head>
  <body>  
  @include ('layouts.nav') 

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div id="app" class="jumbotron">
      
      @yield('content')

      @include('layouts.footer')
    </div> <!-- /container -->   
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->    
    @if(Request::is('register') || Request::is('contact')) 
    <script src='https://www.google.com/recaptcha/api.js'></script>  
    @endif
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery.min.js"><\/script>')</script>    
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
       
  </body>
</html>
