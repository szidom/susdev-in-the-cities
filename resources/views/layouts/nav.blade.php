
 <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-selfmade">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      @if(Request::is('/'))
      <a class="navbar-brand" href="#picopen_logo" data-toggle="modal" data-target="#picopen_logo">
        <img height=40px src="/storage/additional_files/transparent_logo.png" alt="VihreaKaupunki" />
      </a>
      @else
      <a class="navbar-brand" href="/"><img height=40px src="/storage/additional_files/transparent_logo.png" alt="VihreaKaupunki" /></a>
      @endif
      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
           @if (Route::has('login'))
           @if (Auth::check())
          <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" id="dropdown02" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }}
            </a>
                <div class="dropdown-menu" role="menu" aria-labelledby="dropdown02">
                    <a class="dropdown-item" href="{{url('/settings')}}">Tiedot<span class="sr-only">(current)</span></a>
                    <a class="dropdown-item" href="{{ url('/assignment') }}">Uusi tehtävänanto</a>
                    <a class="dropdown-item" href="{{ url('/projects') }}">Projektini</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Kirjaudu ulos</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                    </form>
                </div>     
          </li>
          @else
          <li class="nav-item {{ setActive('login') }}" >
             <a class="nav-link" href="{{ url('/login') }}">Kirjaudu</a>
          </li>
          <li class="nav-item {{ setActive('register') }}" >
            <a class="nav-link" href="{{ url('/register') }}">Rekisteröidy</a>
          </li>
          @endif
          @endif
          <!--This is a drop down template:
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="/references">References</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>  -->
          <li {{ setActiveWithClass('references') }}>
            <a class="nav-link" href="/references">Projektit</a>
          </li>
          <li {{ setActiveWithClass('designs') }}>
            <a class="nav-link" href="/designs">Suunnitelmat</a>
          </li>
          <li {{ setActiveWithClass('designers') }}>
            <a class="nav-link" href="/designers">Suunnittelijat</a>
          </li>
          <li {{ setActiveWithClass('intro') }}>
            <a class="nav-link" href="/intro">Johdatus Permakulttuuriin</a>
          </li>
          <li {{ setActiveWithClass('contact') }}>
            <a class="nav-link" href="/contact">Kontakti</a>
          </li>
        </ul>        
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    @if(Request::is('/'))
     <div class="modal fade" id="picopen_logo" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" rofutoversenzle="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Vihreä Kaupunki</h4>
                                </div>
                                <div class="modal-body w3-center">
                                    <img class="w3-image" src="storage/additional_files/original_VIHRE_KAUPUNKI.png" alt="VihreaKaupunki"><br>
                                </div>
                                <div class="modal-footer">
                                <button type="button" 
                                    class="btn btn-default" 
                                    data-dismiss="modal"
                                    style="float:left;">Close</button>
                                </div>
                            </div>    		
                        </div>
                </div>
    @endif
