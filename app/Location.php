<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
     use SoftDeletes;
    //
    
    protected $table = 'locations';
    
    protected $fillable = ['name', 'address','lat','lng']; 
    
    public function Project()
    {
        return $this->hasOne('App\Project', 'located');
    }   
    
}
