<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Design extends Model
{
    use SoftDeletes;
    
    protected $table = 'designs';

    protected $fillable = ['description', 'project'];
    
    
    public function Photos()
    {
        return $this->hasMany('App\Photo', 'design');
    }
    
    public function Project()
    {
        return $this->belongsTo('App\Project', 'project');
    }   
}
