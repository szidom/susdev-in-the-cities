<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class InformationPage extends Model
{
    use Notifiable;
    use SoftDeletes;
    
    protected $table = 'information_pages';
    
    protected $fillable = ['title', 'content', 'references', 'editor','pageNumber',];    
    //
    public function Editor()
    {    
        return $this->hasOne('App\Designer', 'editor');
    }
    
    public function Photos()
    {
        return $this->hasMany('App\Photo', 'informationPage');
    }
    
    public function getContentWithPics(){
        if($this->Photos()->count()){
                foreach($this->Photos()->get() as $photo){
                    $str="</p>
                    <a href=\"#picopen_".$photo->id."\" data-toggle=\"modal\"
                                data-target=\"#picopen_{$photo->id}\"
                                title=\"Open {$photo->alt}\">                         
                    
                    <img class=\"w3-image-small\" src=\"".$photo->location."\" alt=\"".$photo->alt."\"></a>";
                    $str .=
                    "<div class=\"modal fade\" id=\"picopen_{$photo->id}\" tabindex=\"1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
                        <div class=\"modal-dialog modal-lg\" rofutoversenzle=\"document\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <h4 class=\"modal-title\">{$photo->alt}</h4>
                                </div>
                                <div class=\"modal-body w3-center\">
                                    <img class=\"w3-image\" src=\"".$photo->location."\" alt=\"".$photo->alt."\"><br>
                                </div>
                                <div class=\"modal-footer\">
                                <button type=\"button\" 
                                    class=\"btn btn-default\" 
                                    data-dismiss=\"modal\"
                                    style=\"float:left;\">Close</button>
                                </div>
                            </div>    		
                        </div>
                    </div>
                    <p class=\"w3-text\">
                    "; 
                    
                    $change="[".$photo->number."]";                
                    $this->content = str_replace($change, $str, $this->content );                               
                }
            }
         return $this;
    }
}
