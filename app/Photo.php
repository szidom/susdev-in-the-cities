<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use SoftDeletes;
    //    
    protected $table = 'photos';
    
    protected $fillable=['alt', 'location', 'number', 'project', 'informationPage', 'design'];
    
    public function Customer()
    {
        return $this->hasOne('App\User', 'avatar');    
    }
    
    public function Designer()
    {
        return $this->hasOne('App\Designer', 'avatar');
    }
    
    public function Project()
    {
        return $this->belongsTo('App\Project', 'project');
    }
    
    public function Design()
    {
        return $this->belongsTo('App\Design', 'design');
    }
    
    public function InformationPage()
    {
        return $this->belongsTo('App\InformationPage',  'informationPage');
    }
    
}
