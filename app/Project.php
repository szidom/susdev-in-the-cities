<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use SoftDeletes;
    use Notifiable;
    
    protected $table = 'projects';

    protected $fillable = ['name', 'customer', 'designer', 'located', 'description', 'isReference', 'isImplemented', 'onGoing','isNew'];
    

    
    public function Customer()
    {
        return $this->belongsTo('App\User', 'customer');
    }
    
    public function Designer()
    {
        return $this->belongsTo('App\Designer', 'designer');
    }
    
    public function Location()
    {
        return $this->belongsTo('App\Location', 'located');
    }
    
    public function Photos()
    {
        return $this->hasMany('App\Photo', 'project');
    }
    
    public function Follower()
    {
        return $this->hasOne('App\Follower', 'project_id');
    }
    
    public function Design()
    {
        return $this->hasOne('App\Design', 'project');
    }
    
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
