<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    use SoftDeletes;
    use Notifiable;
    
    protected $table = 'followers';
    
    protected $fillable = ['number', 'project_id',];
    
    public function Project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }
    
    public function Users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }
    
    public function addFollower($user)
    {        
        if(!($this->Users()->get()->contains('id', $user->id)))
        {        
            $this->Users()->attach($user);
            $this->number++;
            $this->save();
        }
    }
}
