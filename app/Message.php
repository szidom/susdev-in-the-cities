<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use Notifiable;
    use SoftDeletes;    
    //
    protected $table = 'messages';    
    
    protected $fillable = ['toTitle', 'subject' ,'message', 'fromTitle', 'isFromDesigner',];
    
    //In frontend to is always a Designer, and from is always a User
    /*
    public function Sender(){
        if($this->isFromDesigner)
            return $this->hasOne('App\Designer', 'from');
        else
            return $this->hasOne('App\User', 'from');
    }
    
    public function Reciever(){
        if($this->isFromDesigner)
            return $this->hasOne('App\User', 'to');
        else
            return $this->hasOne('App\Designer', 'to');
    }*/
    
}
