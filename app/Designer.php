<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'intro', 'isShown', 'phone', 'avatar', 'surname', 'lastname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    public function designedProjects()
    {
        return $this->hasMany('App\Project', 'designer');
    }
    
    public function Avatar()
    {
        return $this->belongsTo('App\Photo', 'avatar');
    }
    
    public function editedPages()
    {
        return $this->hasMany('App\InformationPage', 'addedBy');
    }
    // overrided functions to make it readonly!
    protected static $carbonFields = [];
 
    public function save(array $options = []){
        return false;
    }
    
    public function update(array $attributes =[], array $options =[]){
        return false;
    }
  
    static function firstOrCreate(array $arr){
        return false;
    }
  
    static function firstOrNew(array $arr){
        return false;
    }
  
    public function delete(){
        return false;
    }
  
    static function destroy($ids){
        return false;
    }
  
    public function restore(){
        return false;
    }
  
    public function forceDelete(){
        return false;
    }
  
    /* We need to disable date mutators, because they brake toArray function on this model */
    public function getDates(){
        return array();
    }
}
