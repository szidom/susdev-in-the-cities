<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\Traits\CaptchaTrait;

class ContactController extends Controller
{
    use CaptchaTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guest.contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['captcha'] = $this->captchaCheck();
        //dd($request->all());
        $this->validate($request,[
            'fromTitle' => 'required|string|email|max:255',
            'message' => 'required|min:5|string',
            'subject' => 'required|string|max:255',
            'g-recaptcha-response'  => 'required',
            'captcha'               => 'required|min:1'        
        ]);
        $message=new Message($request->all());
        $message->save();
        return back()->withError(['Message has been sent.']);
        // $message->isFromDesigner set to true on backend-means message has been red.
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
