<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Photo;
use File;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function destroy(Photo $photo)
    {
        File::delete($photo->location);
        $photo->delete();
        //dd("bsams");
        return back();
    }
}
