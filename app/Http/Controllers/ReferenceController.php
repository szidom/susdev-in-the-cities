<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Design;
use DB;
use Auth;

class ReferenceController extends Controller
{
    //
    public function getReferences()
    {
        $references=Project::where('isReference', '=', '1')->get();
        foreach($references as $ref)
        {
            $ref->description=\Illuminate\Support\Str::words($ref->description, 12);        
        }
        return view('guest.references', ['references'=>$references]);
    
    }
    
    public function getImplemented()
    {
        $implemented=getReferences()->where('isImplemented', '=', '1');    
        return view('guest.implemented', ['implemented' => $implemented]);
    }
    
    public function show(Project $ref)
    {   
        /*if($ref->Location()->count()){
            $location=$ref->Location()->first();
            JavaScript::put([
                'loc' => $location
            ]);
        }*/
        if($ref->isReference){
            $design = $ref->Design()->first();
            return view('guest.showreference', compact('ref', 'design'));
        }
        else if($user = Auth::user()){
            if($ref->customer===$user->id){
                $design = $ref->Design()->first();
                return view('guest.showreference', compact('ref', 'design'));
            }
        }
        else{
            $project=new Project([
                'name' => 'Not Found Project',
                'description' => 'The project does not exists',
                'designer' => null
            ]);
            
            return view('guest.showreference', ['ref' => $project]);
        }
    }
    
    public function showDesign(Design $design)
    {       
        $ref = $design->Project()->first(); 
        if($ref->isReference && !$ref->onGoing){
            return view('guest.showdesign', compact('ref', 'design'));
        }
        else if($user = Auth::user()){
            if($ref->customer===$user->id){
                return view('guest.showdesign', compact('ref', 'design'));
            }
        }
        else{
            $project=new Project([
                'name' => 'Not Found Project',
                'description' => 'The project does not exists',
                'designer' => null
            ]);            
            return view('guest.showreference', ['ref' => $project]);
        }
    }
    
    public function getDesigns()
    {  
        $designs=Design::whereExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('projects')
                      ->where('isReference','=','1')
                      ->where('onGoing', '=', '0')
                      ->whereRaw('designs.project = projects.id');
            })->with('project')->get();
        foreach($designs as $ref)
        {
            $ref->description=\Illuminate\Support\Str::words($ref->description, 12);        
        }         
        return view('guest.designs', ['references'=>$designs]);      
    }
    
    public function searchReference(Request $request)
    {
        $query=$request->query;
        $keyword=$request->keyword;      
        
    }
}
