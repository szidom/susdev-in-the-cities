<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Photo;
use File;
use Image;

class UserController extends Controller
{
    //Autentikáció
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= Auth::user();
        return view('loggedIn.editUser', ['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    
        $user = Auth::user();
        //validation
        $valArray=[
            'email' => 'required',
            'name' => 'required|max:25',
            'password' => 'required',
            'phone' => 'digits_between:9,14|nullable',
            'photo' => 'max:2000',
        ];
        
        if(Hash::check($request->password, $user->password)){
            if(!empty($request->new_password)){
                $valArray['new_password'] = 'required|min:6|confirmed';
                $valArray['new_password_confirmation'] = 'required';
            }
            $this->validate($request, $valArray);
            //dd("cucimuci", $valArray, $request->except('password'));
        
        
            if(!empty($request->new_password)){
                if($request->new_password === $request->new_password_confirmation){
                    $user->password = bcrypt($request->new_password);
                }
            }
            
            if($request->hasFile('photo')){                   
                if($user->avatar){
                    $tmpphoto = $user->Avatar()->first();
                    File::delete($tmpphoto->location);
                    $tmpphoto->delete();
                }
                
                $img = Image::make($request->file('photo'));
                $path = 'storage/commonfiles/avatars/'.$request->file('photo')->getClientOriginalName();
                if($img->filesize()>100000){
                    $img->fit(380, 220);
                }
                $img->save($path);
                            
                $photo = new Photo([
                    'alt' => $user->name,
                    'location' => $path,
                ]);  
                $photo->save();        
                        
                $user->avatar=$photo->id; 
            }
            $user->update($request->except('password'));
            
            return back();
        }
            
        $this->validate($request, $valArray);
        return back()->withErrors(["Password is not correct."]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }    
    
    public function removeAvatar(Photo $photo)
    {
        $user = Auth::user();
        $user->avatar=null;
        $user->save();
        
        File::delete($photo->location);
        $photo->delete();
        //dd("bsams");
        return back();
    }
}
