<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Image;
use App\Project;
use App\Location;
use App\Photo;

class ProjectController extends Controller
{
    //Autentikáció
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    
    public function showAddPage(){
        return view('loggedIn/assign');
    }
    
    public function add(Request $request)
    {       
        //dd($request->all());
         $valArray = [
            'name' => 'required|unique:projects|max:50',
            'description' => 'required|min:25',
            'photo' =>'max:2000',
            'address' => 'required',
            'geolat' => 'required|regex:/^-?\d{1,2}\.\d{6,}$/', //is float check
            'geolng' => 'required|regex:/^-?\d{1,2}\.\d{6,}$/',
        ];
        
        if($request->hasFile('photo1')){
            $newNumOfPhoto = $request->input('newNumOfPhoto');      
            if($newNumOfPhoto>0){              
                for($i=1;$i<=$newNumOfPhoto;++$i){
                    $valArray['photo'.$i] = 'max:2000';                    
                }
            }
        }
        $this->validate($request, $valArray);
        
        if($request->input('geolat')!=null && $request->input('geolng')!=null){
            $location=new Location([
                'lat' => $request->input('geolat'),
                'lng' => $request->input('geolng'),
                'name' => $request->input('name'),
                'address' => $request->input('address'),
            ]);        
            $location->save();            
        }               
        $user = Auth::user();
        
        $project=new Project([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'isNew' => true,
        ]);   
        
        if(!empty($location))
            $project->located = $location->id; 
        
        $project->customer = $user->id;       
        $project->save(); 
        
        if ($request->hasFile('photo1'))
        {
           
           $newNumOfPhoto = $request->input('newNumOfPhoto');      
            if($newNumOfPhoto>0){                 
                for($i=1; $i<=$newNumOfPhoto; ++$i){
                    if($request->hasFile('photo'.$i)){
                        $img = Image::make($request->file('photo'.$i));
                        $path = 'storage/commonfiles/projects/'.$request->file('photo'.$i)->getClientOriginalName();
                        //greater then 1Mb files will be reduced.
                        if($img->filesize()>1000000){
                            $img->fit(1366,768);
                        }
                        $img->save($path);
                        
                        $photo = new Photo([
                            'alt' => $request->input('alt'.$i),
                            'location' => $path,
                            'project' => $project->id,
                            'number' => $i,
                        ]);  
                        $photo->save();    
                    }       
                }
            }
        }
        return back();      
    }   
    
    public function index()
    {
        $user= Auth::user();
        $projects=Project::where('customer','=', $user->id)->paginate(1);        
        //return view('projects/show',['projects' => $projects, 'project' => $project ]); 
        //dd($projects);
        return view('projects.index', ['projects' => $projects]);
    }
    
    public function update(Request $request, Project $project){
    
        $valArray=[
            'name' => 'bail|required|max:25',
            'description' => 'required|min:25',
            'address' => 'required',            
        ];
        
        $oldNumOfPhoto= empty($project->Photos()) ? 0 : $project->Photos()->count() ;
        $newNumOfPhoto = $request->input('newNumOfPhoto');
        if($request->hasFile('photo'.($oldNumOfPhoto+1))){          
            for($i=$oldNumOfPhoto;$i<=$newNumOfPhoto;++$i){
                //dd("vankep");
                $valArray['photo'.$i] = 'max:2000';                    
            }
        }
        $this->validate($request, $valArray);
        
        if($request->hasFile('photo'.($oldNumOfPhoto+1))){                   
            for($i=$oldNumOfPhoto+1; $i<=$newNumOfPhoto; ++$i){
                if($request->hasFile('photo'.$i)){
                    $img = Image::make($request->file('photo'.$i));
                    $path = 'storage/commonfiles/projects/'.$request->file('photo'.$i)->getClientOriginalName();
                    //greater then 1Mb files will be reduced.
                    if($img->filesize()>1000000){
                        $img->fit(1366,768);
                    }
                    $img->save($path);
                        
                    $photo = new Photo([
                        'alt' => $request->input('alt'.$i),
                        'location' => $path,
                        'project' => $project->id,
                        'number' => $i,
                    ]);  
                    $photo->save();          
                }
            }
        }
                
        $location=$project->Location()->first();
        $location->update($request->all());
        $project->isNew=true;
        $project->update($request->all());
        
        return back();
    }
    
}
