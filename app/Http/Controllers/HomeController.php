<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InformationPage;
use App\Project;
use App\Designer;

class HomeController extends Controller
{
    
    public function index()
    {
        return view('home');
    }
    
    public function dashboard()
    {
        $intro = InformationPage::get()->sortBy('pageNumber')->first();
        if(empty($intro)){
            $intro=new InformationPage([
                'title' => 'Under Construction',
                'content' => 'Currently there is no available content. Visit us later, please.',
                'pageNumber' => 1,
            ]);
        }
            
        $intro->content = \Illuminate\Support\Str::words($intro->content, 52);
        
        $refs = Project::where('isReference', '=', '1')->get()->sortByDesc('updated_at');
        $refs->splice(3);
        foreach($refs as $ref)
        {
            $ref->description = \Illuminate\Support\Str::words($ref->description, 21);        
        }
        return view('index', ['intro'=>$intro, 'refs'=>$refs]);
    }
    
    public function listingDesigners()
    {
        $designers=Designer::where('isShown', '=', '1')->get();  
        foreach($designers as $item)
        {
            $item->intro=\Illuminate\Support\Str::words($item->intro, 20);        
        } 
        return view('guest.designers', ['designers' => $designers]); 
    }
}
