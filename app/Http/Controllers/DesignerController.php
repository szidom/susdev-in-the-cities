<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Designer;

class DesignerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function showDesigner($id)
    {
        $designer=Designer::find($id);
        
        return view('loggedIn.designer', ['designer' => $designer ]);
    }   
}
