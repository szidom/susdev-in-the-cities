<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InformationPage;

class InformationPageController extends Controller
{
    public function listingInfos()
    {
        $info_pages=InformationPage::get()->sortBy('pageNumber');
        foreach($info_pages as $ip){
            $ip = $ip->getContentWithPics(); 
        }
                
        return view('guest.intro', ['info_pages'=>$info_pages]);          
    }
}
