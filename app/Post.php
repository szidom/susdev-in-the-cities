<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Notifiable;
    use SoftDeletes;
    //
    protected $table = 'posts';
    
    protected $fillable = ['content', 'editor'];
    
    public function Editor()
    {
        return this->belongsTo('App\User', 'editor');
    }
    
    public function Comments()
    {
        return $this->hasMany('App\Comment','mainPost');
    }
    
}
