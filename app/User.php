<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar' , 'surname', 'lastname', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function orderedProjects()
    {
        return $this->hasMany('App\Project', 'customer');
    }
    
    public function Avatar()
    {
        return $this->belongsTo('App\Photo', 'avatar');
    }
    
    public function followedProjects()
    {
        return $this->belongsToMany('App\Follower')->withTimestamps();
    }
}
