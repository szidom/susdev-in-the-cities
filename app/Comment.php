<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{    
    use Notifiable;
    use SoftDeletes;
    //
    protected $table = 'comments';
    
    protected $fillable = ['content', 'editor', 'mainPost'];
    
    public function Post()
    {
        return $this->belongsTo('App\Post', 'mainPost');
    }
    
}
