<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->default(null);
            $table->integer('project')->unsigned();
            //$table->foreign('project')->references('id')->on('susdev_db.projects');
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('designs', function($table) 
        {
            $table->foreign('project')->references('id')->on('projects'); 
        });
        
        Schema::table('photos', function($table) 
        {
            $table->foreign('design')->references('id')->on('designs'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designs');
    }
}
