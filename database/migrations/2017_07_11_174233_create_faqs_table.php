<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->string('answer');
            $table->integer('addedBy')->unsigned()->nullable();
            // $table->foreign('addedBy')->references('id')->on(Schema::table('designers');
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('faqs', function($table) {
            $table->foreign('addedBy')->references('id')->on('designers');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
