<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->integer('project_id')->unsigned()->nullable();
            //$table->foreign('project_id')->references('id')->on('susdev_db.projects');
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('followers', function($table) {
            $table->foreign('project_id')->references('id')->on('projects');
        });
        
        Schema::create('follower_user', function (Blueprint $intertable){
            $intertable->increments('id');
            $intertable->integer('follower_id')->unsigned()->nullable();
            //$intertable->foreign('follower_id')->references('id')->on('susdev_db.followers');
            $intertable->integer('user_id')->unsigned()->nullable();
            //$intertable->foreign('user_id')->references('id')->on('susdev_db.users');            
            $intertable->timestamps();
            $intertable->softDeletes();
        });
        
        Schema::table('follower_user', function($table) {
            $table->foreign('follower_id')->references('id')->on('followers');
            $table->foreign('user_id')->references('id')->on('users'); 
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('follower_user');
        Schema::dropIfExists('followers');
    }
}
