<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('content');
            $table->integer('editor')->unsigned()->nullable();
            //$table->foreign('editor')->references('id')->on('susdev_db.users');
            $table->integer('mainPost')->unsigned()->nullable();
            //$table->foreign('mainPost')->references('id')->on('susdev_db.posts');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('comments', function($table) {
            $table->foreign('mainPost')->references('id')->on('posts');
            $table->foreign('editor')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
