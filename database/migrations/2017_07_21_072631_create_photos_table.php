<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alt');
            $table->string('location');
            $table->integer('number')->default(0);         
            $table->integer('project')->unsigned()->nullable();          
            //$table->foreign('project')->references('id')->on('susdev_db.projects');            
            $table->integer('informationPage')->unsigned()->nullable();        
            //$table->foreign('informationPage')->references('id')->on('susdev_db.information_pages');     
            $table->integer('design')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('photos', function($table) {
            $table->foreign('project')->references('id')->on('projects');
            $table->foreign('informationPage')->references('id')->on('information_pages');     
        });
        
        Schema::table('users', function($table) 
        {
            $table->foreign('avatar')->references('id')->on('photos');
        });
        
        Schema::table('designers', function($table) 
        {
            $table->foreign('avatar')->references('id')->on('photos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
