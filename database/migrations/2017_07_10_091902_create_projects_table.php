<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->integer('customer')->unsigned();
           // $table->foreign('customer')->references('id')->on('pnvbzigx_susdev_db.users');
            $table->integer('designer')->unsigned()->nullable();
            //$table->foreign('designer')->references('id')->on('pnvbzigx_susdev_db.designers');
            $table->integer('located')->unsigned()->nullable();
            $table->text('description');
            $table->boolean('isReference')->default(0);
            $table->boolean('isImplemented')->default(0);
            $table->boolean('onGoing')->default(0);
            $table->boolean('isNew')->default(1);    
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('projects', function($table) {
            $table->foreign('customer')->references('id')->on('users');
            $table->foreign('designer')->references('id')->on('designers');
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
