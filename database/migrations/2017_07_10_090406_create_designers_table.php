<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname')->nullable()->default(null);
            $table->string('lastname')->nullable()->default(null);            
            $table->string('email')->unique();
            $table->string('password');
            $table->longtext('intro')->nullable()->default(null);
            $table->boolean('isShown')->default(0);
            $table->string('phone')->nullable()->default(null);          
            $table->integer('avatar')->unsigned()->nullable();            
            $table->softDeletes();  
            $table->rememberToken();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designers');
    }
}
