<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('content');
            $table->integer('editor')->unsigned()->nullable();
            //$table->foreign('editor')->references('id')->on('susdev_db.users');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('posts', function($table) {
            $table->foreign('editor')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
