<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longtext('content');
            $table->integer('pageNumber')->default(0);
            $table->string('references')->nullable()->default(null);
            $table->integer('editor')->unsigned()->nullable();
            //$table->foreign('editor')->references('id')->on('susdev_db.designers');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('information_pages', function($table) {
            $table->foreign('editor')->references('id')->on('designers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_pages');
    }
}
