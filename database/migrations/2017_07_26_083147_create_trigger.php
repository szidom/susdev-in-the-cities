<?php

use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER tr_Follower_Default_Member_Project AFTER INSERT ON `projects` FOR EACH ROW
            BEGIN
                INSERT INTO followers (`number`, `project_id`, `created_at`, `updated_at`) 
                VALUES (1, NEW.id, now(), null);
            END
        ');
        
        DB::unprepared('
        CREATE TRIGGER tr_User_Default_Member_Follower AFTER INSERT ON `followers` FOR EACH ROW
            BEGIN
                INSERT INTO follower_user (`follower_id`, `user_id`, `created_at`, `updated_at`) 
                VALUES (NEW.id, 1, now(), null);
            END
        ');
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        DB::unprepared('DROP TRIGGER `tr_Follower_Default_Member_Project`');
        DB::unprepared('DROP TRIGGER `tr_User_Default_Member_Follower`');        
    }
}
