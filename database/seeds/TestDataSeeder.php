<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           'name' => 'Szuper User',
           'email' => 'user@user.hu',
           'password' => bcrypt('jelszo'),
        ]);
        
        DB::table('users')->insert([
           'name' => 'Mata Hari',
           'email' => 'muci@cuci.hu',
           'password' => bcrypt('jelszo1'),
        ]);
        
        DB::table('locations')->insert([
           'name' => 'Location',
           'address' => 'Livonsaari, Pohjanpaantie 67., Finland',
           'lat' => '60.5170634',
           'lng' => '21.7596523',
        ]);
        
        DB::table('projects')->insert([
           'name' => 'Szuper Project',
           'customer' => '1',
           'designer' => '1',
           'located' => '1',
           'description' => 'Ez egy PROJECT bemutatkozo szoveg. \n Erted?',
        ]);
        
        DB::table('projects')->insert([
           'name' => 'Referencia Project',
           'customer' => '1',
           'designer' => '1',
           'description' => 'Ez egy Referencia bemutatkozo szoveg. /n Erted?',
           'isReference'=> true,
           'located'=>'1',
           'isNew' => false,
        ]);
        
        DB::table('projects')->insert([
           'name' => 'Referencia Project 2',
           'customer' => '1',
           'designer' => '1',
           'description' => 'Ez a 2. Referencia bemutatkozo szoveg. /n Erted?',
           'isReference'=> true,
           'located'=>'1',
           'isNew' => false,
        ]);
        
        DB::table('projects')->insert([
           'name' => 'Referencia Project 3',
           'customer' => '1',
           'designer' => '1',
           'description' => 'Ez a 3. Referencia bemutatkozo szoveg. /n Erted?',
           'isReference'=> true,
           'located'=>'1',
           'isNew' => false,
        ]);
        
        DB::table('projects')->insert([
           'name' => 'Referencia Project 4',
           'customer' => '1',
           'designer' => '1',
           'description' => 'Ez a 4. Referencia bemutatkozo szoveg. /n Erted?',
           'isReference'=> true,
           'located'=>'1',
           'isNew' => false,
        ]);
        
        DB::table('posts')->insert([
           'content' => 'Post egy Szuper Project felhasznalojatol...',
           'editor' => '1',
        ]);
        
        DB::table('comments')->insert([
           'content' => 'Comment egy Szuper Project felhasznalojatol...',
           'editor' => '1',
           'mainPost'=> '1',
        ]); 
        
        DB::table('information_pages')->insert([
            'title'=>'info',
            'content'=>'greed joy vanity regret met may ladies oppose who. Mile fail as left as hard eyes. Meet made call in mean four year it to. Prospect so branched wondered sensible of up.',
            'references'=>'dasd',
            'editor'=>'1',        
        ]);
        
        DB::table('photos')->insert([
            'alt' => 'Keni',
            'location' => 'storage/commonfiles/avatars/kenny.jpg',
            'project' => '2',
        ]);
        
        DB::table('photos')->insert([
            'alt' => 'Ref projekt kep',
            'location' => 'storage/commonfiles/references/capitalism.jpg',
            'project' => '3',
        ]);      
        
        DB::table('photos')->insert([
            'alt' => 'sdsd',
            'location' => 'storage/commonfiles/references/smile2beer.jpg',
            'project' => '4',
            'informationPage'=>'1',
        ]); 
        
        DB::table('photos')->insert([
            'alt' => 'aaa',
            'location' => 'storage/commonfiles/references/capitalism.jpg',
            'project' => '5',
        ]); 
        
        DB::table('photos')->insert([
            'alt' => 'sdaww',
            'location' => 'storage/commonfiles/references/capitalism.jpg',
            'project' => '4',
        ]); 
    }
}
